'use strict';

angular
  .module('yapp', [
    'ui.router'
    , 'ui.bootstrap'
    , 'ngAnimate'
    , 'darthwade.dwLoading'
    , 'fusioncharts'
    , 'angularUtils.directives.dirPagination',
    , 'angular-svg-round-progress'
    , 'chart.js'
  ])
  .constant('URL', {
    localURL: 'http://tweetanalyst.test.local/',
    testURL: 'https://ta-api.test.local/services/'
  })
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when('/dashboard/:id', '/dashboard/:id/overview');
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
      })
      .state('login', {
        url: '/login',
        parent: 'base',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .state('dashboard', {
        url: '/dashboard/:id',
        parent: 'base',
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .state('overview', {
        url: '/overview',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/overview.html'
      })
      .state('reports', {
        url: '/reports',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/reports.html',
      })
      .state('compareUsers', {
        url: '/compareUsers',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/compareUsers.html',
      })
      .state('group', {
        url: '/group',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/group.html',
        controller: 'groupCtrl'
      })
      .state('analyse', {
        url: '/analyse/:tweetId',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/analyse.html',
        controller: 'TweetCtrl'
      });

  });

