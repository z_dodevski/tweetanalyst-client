'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl',['$scope', '$state', '$stateParams', '$http', '$loading','URL', function($scope, $state, $stateParams, $http,  $loading, URL) {

    $scope.$state = $state;
    $loading.start('view');

    $scope.ComparedUser = null;
    //SplashLoader.open();
    $http.get(URL.localURL + 'api/User/Get', {
      params: {
        user_id: $stateParams.id
      }
    }).then(function (response) {
      $scope.User = response.data;
      $loading.finish('view');
     // SplashLoader.close();
    });

    $http.get(URL.localURL  + '/api/Tweet/GetUserTimelineTweets', {
      params: {
        user_id: $stateParams.id
      }
    }).then(function (response) {
      $scope.Statuses = response.data;
    });


    $scope.goToUser = function (id) {
      $state.transitionTo("dashboard", {id: id});
    }

    $scope.goToTweet = function (id) {
      $state.transitionTo("analyse", {id: $stateParams.id, tweetId: id});

    }

    $scope.analyseTweets = function () {

      if(typeof $scope.combiChart == 'undefined')
      {
      $loading.start('view');
        $scope.waitForTweets = true;
      }
      else {
        $state.go("reports");
      }

    };


    $scope.searchUsers = function(val) {
      return $http.get(URL.localURL + 'api/User/Search', {
        params: {
          query : val,
          page : 1,
          count: 10
        }
      }).then(function(response){
        $scope.Users = response.data;
        return response.data;
      });
    };

    $scope.onSelect = function ($item) {
      $loading.start('view');
      $http.get(URL.localURL + 'api/User/Compare', {
        params: {
          firstUser_Id: $stateParams.id,
          secondUser_id : $item.id
        }
      }).then(function (response) {

        $scope.similarityResult = response.data;
        $scope.current = (response.data.similarityResult.similarity) * 100;
        $scope.ComparedUser = response.data.secondUser;
        $loading.finish('view');
      });

    };

   $http.get(URL.localURL + 'api/Reports/GetYearly', {
      params: {
        user_id: $stateParams.id
      }
    }).then(function (response) {

      $scope.emotionsChartModel = response.data.emotionsChartModel;
      $scope.dailyTweetsChartModel = response.data.dailyTweetsChartModel;
      $scope.taxonomyChartModel = response.data.taxonomyChartModel;
      $scope.combiChart = response.data.columnChartModel;
      if($scope.waitForTweets == true)
      {
        $state.go("reports");
        $loading.finish('view');
      }
    });

    $scope.compareUser = function (id) {

      $http.get(URL.localURL + 'api/User/CompareUsers', {
        params: {
          firstUser_Id: $stateParams.id,
          secondUser_Id: id
        }
      }).then(function (response) {
        $scope.UserComparison = response.data;

      });
    };

    $scope.downloadFile = function(downloadType, tweetId) {
      var downloadPath = '';

      $http.get(URL.localURL + 'api/Reports/DownloadFilePath', {
        params: {
          downloadType: downloadType,
          user_id: $stateParams.id, 
          tweet_id: tweetId
        }
      }).then(function (response) {
          downloadPath = response.data;
          window.open(downloadPath);
      });


    }


  }]
  );
