
angular.module('yapp')
  .controller('groupCtrl', function ($scope, $state, $http, URL, $loading) {

    $scope.$state = $state;
    $scope.users = [];

    $scope.searchUsers = function (val) {
      return $http.get(URL.localURL + 'api/User/Search', {
        params: {
          query: val,
          page: 1,
          count: 10
        }
      }).then(function (response) {
        $scope.Users = response.data;
        return response.data;
      });
    };

    $scope.Remove = function (item) {
      $scope.users.splice(item, 1);
    }

    $scope.onSelect = function ($item) {
      $scope.users.push($item);
      $scope.asyncSelected = "";
    };

    $scope.analyzeGroup = function () {
      var listId = [];
      for (var i = 0; i < $scope.users.length; i++) {
        listId.push($scope.users[i].id);
      }
      //listId.push($state.params.id);


        $loading.start('view');
      $http.get(URL.localURL + 'api/Group/GetGroupTimelineTweets', {
        params: {
          userIds: listId.toString()
        }
      }).then(function (response) {
        $scope.Group = response.data;
        $scope.Statuses = response.data.statuses;
        $scope.taxonomyChartModel = response.data.report.taxonomyChartModel;
        $scope.dailyTweetsChartModel = response.data.report.dailyTweetsChartModel;
        $scope.combiChart = response.data.report.columnChartModel;
        $scope.emotionsChartModel = response.data.report.emotionsChartModel;
            $loading.finish('view');
      });

    };


  });
