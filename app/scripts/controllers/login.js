'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('LoginCtrl', ['$scope', '$location', '$http', '$state','URL', function($scope, $location, $http, $state, URL) {

    $scope.searchUsers = function(val) {
        return $http.get(URL.localURL + 'api/User/Search', {
          params: {
            query : val,
            page : 1,
            count: 5
          }
        }).then(function(response){
          $scope.Users = response.data;
          return response.data;
        });
    };

    $scope.onSelect = function ($item) {
      $scope.Users = [];
      $scope.Users.push($item);
      $state.transitionTo("dashboard", { id: $item.id});
    };


    }
  ]);
