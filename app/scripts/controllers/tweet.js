/**
 * Created by Zlate on 1/30/2016.
 */


angular.module('yapp')
  .controller('TweetCtrl', ['$scope', '$state', '$stateParams', '$http','$loading', 'URL', function($scope, $state, $stateParams, $http, $loading, URL) {

    $scope.$state = $state;


    $scope.labels = [];

    $scope.data = [ ];

    $loading.start('view');
    $http.get(URL.localURL + 'api/Tweet/Get', {
      params: {
        id : $stateParams.tweetId
      }
    }).then(function(response){
      $scope.Tweet = response.data;
      $scope.labels = response.data.classificationChartData.labels;
      $scope.data.push(response.data.classificationChartData.dataSet);

      $scope.emotionsHeaders = ["Anger", "Disgust", "Fear", "Joy", "Sadness"];
      $scope.emotions = response.data.alchemyEmotion.emotions;

      $scope.categories = response.data.alchemyTaxonomy.categories;
      $scope.presence = response.data.alchemyTaxonomy.presence;

      $loading.finish('view');

    });


    }]
  );
